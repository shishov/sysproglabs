
// ShishovLab1Dlg.h : header file
//

#pragma once
#include "afxwin.h"


// CShishovLab1Dlg dialog
class CShishovLab1Dlg : public CDialogEx
{
// Construction
public:
	CShishovLab1Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_SHISHOVLAB1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	void TryCreateThread();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	CListBox listBox;
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	int nStartThreads;
};
