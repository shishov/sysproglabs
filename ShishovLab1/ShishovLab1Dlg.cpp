
// ShishovLab1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "ShishovLab1.h"
#include "ShishovLab1Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAX_THREADS 200

PROCESS_INFORMATION processInformation;
HANDLE hCreateThreadEvent = CreateEvent(NULL, FALSE, FALSE, _T("CreateThread"));
HANDLE hEventCreated = CreateEvent(NULL, FALSE, FALSE, _T("ThreadCreated"));

HANDLE hTerminateThreadEvent = CreateEvent(NULL, FALSE, FALSE, _T("TerminateThread"));
HANDLE hEventTerminated = CreateEvent(NULL, FALSE, FALSE, _T("ThreadTerminated"));
HANDLE hEventTerminated1 = CreateEvent(NULL, FALSE, FALSE, _T("ThreadTerminated"));

HANDLE hTerminateProcess = CreateEvent(NULL, FALSE, FALSE, _T("TerminateProcess"));
HANDLE hProcessTerminated = CreateEvent(NULL, FALSE, FALSE, _T("ProcessTerminated"));

int threadCount = 0;
CListBox* pListBox;

// CShishovLab1Dlg dialog
CShishovLab1Dlg::CShishovLab1Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CShishovLab1Dlg::IDD, pParent)
	, nStartThreads(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CShishovLab1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, listBox);
	pListBox = &listBox;
	DDX_Text(pDX, IDC_EDIT1, nStartThreads);
	DDV_MinMaxInt(pDX, nStartThreads, 0, 199);
}

BEGIN_MESSAGE_MAP(CShishovLab1Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CShishovLab1Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CShishovLab1Dlg::OnBnClickedButton2)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CShishovLab1Dlg message handlers

BOOL CShishovLab1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CShishovLab1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CShishovLab1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

DWORD WaitForExit (LPVOID lpdwThreadParam ) 
{	
	DWORD dwThreadCount = 2;
	HANDLE *lpHandles = new HANDLE[dwThreadCount];
	lpHandles[0] = ::hProcessTerminated;
	lpHandles[1] = ::processInformation.hProcess;

	DWORD dwEvent = WaitForMultipleObjects(
			dwThreadCount,
			lpHandles,
			false,
			INFINITE
		);	

	switch(dwEvent)
		{
		case WAIT_OBJECT_0 + 0: 
		case WAIT_OBJECT_0 + 1:
			{	
				while( pListBox->DeleteString(0) );
				CloseHandle( processInformation.hProcess );
				CloseHandle( processInformation.hThread );				
				break;
			}
		default:
			break;
		}

	delete [] lpHandles;
	return 0;
}

void CShishovLab1Dlg::TryCreateThread()
{
	SetEvent( ::hCreateThreadEvent );
	DWORD eventResult = WaitForSingleObject( ::hEventCreated, INFINITE );
	if(eventResult == WAIT_OBJECT_0)
	{
		UpdateData();
		CString t;
		t.Format(_T("Thread %d"), threadCount);
		pListBox->AddString(t);
		threadCount++;				
	}
}

void CShishovLab1Dlg::OnBnClickedButton1()
{
	DWORD dwExitCode = 0;
	GetExitCodeProcess(::processInformation.hProcess,&dwExitCode);

	if(dwExitCode == STILL_ACTIVE)
	{	
		if(threadCount <= MAX_THREADS)
		{
			TryCreateThread();
		}
		else
		{
			AfxMessageBox(_T("Maximum threads number reached"));
		}
	} 
	else
	{
		STARTUPINFO startupInfo;
		memset(&::processInformation, 0, sizeof(::processInformation));
		memset(&startupInfo, 0, sizeof(startupInfo));
		startupInfo.cb = sizeof(startupInfo);

		TCHAR czCommandLine[] = _T("Lab1Console.exe");
		bool result = CreateProcess(0, czCommandLine, 0, 0, FALSE, 0, 0, 0,	
			&startupInfo, &::processInformation);
		if(!result)
		{
			AfxMessageBox(_T("Can't create process"));
			return;
		}

		threadCount = 1;
		
		UpdateData();
		pListBox->AddString(_T("Main Thread"));

		for (int i = 0; i < nStartThreads; i ++)
		{
			TryCreateThread();
		}


		DWORD dwThreadId;
		CreateThread(0,0,(LPTHREAD_START_ROUTINE)&WaitForExit,0,0,&dwThreadId);
	}
}

void CShishovLab1Dlg::OnBnClickedButton2()
{
	if(threadCount > 1)
	{
		SetEvent( ::hTerminateThreadEvent );
		DWORD dwEvent = WaitForSingleObject( ::hEventTerminated, INFINITE );
		if(dwEvent == WAIT_OBJECT_0)
		{
			threadCount--;
			pListBox->DeleteString(listBox.GetCount()-1);
		}
	} 
	else if(threadCount == 1)
	{
		SetEvent( ::hTerminateProcess );
	}
}


void CShishovLab1Dlg::OnClose()
{
	TerminateProcess(::processInformation.hProcess,0);

	CDialogEx::OnClose();
}


void CShishovLab1Dlg::OnDestroy()
{
	CDialogEx::OnDestroy();
		
	TerminateProcess(::processInformation.hProcess,0);
}
