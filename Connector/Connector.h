#include "stdafx.h"
#include <stdexcept>

enum ConnectorState
{
	Disconnected = 0,
	Connected = 1
};

enum MessageCode
{
	Ok, 
	Confirmation,
	NoSuchPeer, 
	CommunicationError
};

enum MessageType
{
	Command = 10,
	TextMessage = 12,
	StatusMessage = 13
};

enum CommandType
{
	CmdCreateThread,
	CmdStopThread,
	CmdStopProcess,
	CmdAskThreads
};

struct MessageHeader
{
	MessageType Type;	
	MessageCode Status;
	DWORD DataLen;
	DWORD ArgLen;
};

struct Message
{	
	~Message();

	static Message TextMessage(void* data, void* arg = 0);
	static Message Command(CommandType type, void* arg = 0);
	static Message StatusMessage(MessageCode code, void* data, void* arg = 0);
	static Message Deserialize(BYTE* data);
	static BYTE* Serialize(Message message);

	MessageHeader Header;	
	BYTE* Data; 
	BYTE* Arg;	
};

class Connector
{
public:
	Connector(void) : State(Disconnected) {};	
	virtual bool Connect(char address[], char peerName[]) {throw new std::exception("Not implemented");};
	virtual bool Send(char address[], Message message, DWORD timeout) {throw new std::exception("Not implemented");};
	virtual bool Receive(Message& message, DWORD timeout) {throw new std::exception("Not implemented");};
	virtual bool Close() {throw new std::exception("Not implemented");};		

protected:
	ConnectorState State;
};
