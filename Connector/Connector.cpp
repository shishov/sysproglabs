// Connector.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "stdafx.h"
#include "Connector.h"

int bytelen(void* body)
{
	if (body == 0)
		return 1;
	return strlen((char*)body) + 1;
}

Message Message::TextMessage(void* data, void* arg)
{
	Message* msg = new Message();
	msg->Data = (BYTE*)data;
	msg->Arg = (BYTE*)arg;	 	

	msg->Header = *(new MessageHeader());	
	msg->Header.Status = MessageCode::Ok;
	msg->Header.Type = MessageType::TextMessage;
	msg->Header.DataLen = bytelen(msg->Data);
	msg->Header.ArgLen = bytelen(msg->Arg);
	return *msg;
}

Message Message::Command(CommandType type, void* arg)
{
	Message* msg = new Message();	
	msg->Data = new BYTE[sizeof(CommandType)];
	sprintf_s( (char*)msg->Data, sizeof(CommandType), "%d", type );
	msg->Arg = (BYTE*)arg;	

	msg->Header = *(new MessageHeader());	
	msg->Header.Status = MessageCode::Ok;
	msg->Header.Type = MessageType::Command;
	msg->Header.DataLen = bytelen(msg->Data);
	msg->Header.ArgLen = bytelen(msg->Arg);
	return *msg;
}

Message Message::StatusMessage(MessageCode code, void* data, void* arg)
{
	Message* msg = new Message();
	msg->Data = (BYTE*)data;	
	msg->Arg = (BYTE*)arg;	

	msg->Header = *(new MessageHeader());	
	msg->Header.Status = code;
	msg->Header.Type = MessageType::StatusMessage;
	msg->Header.DataLen = bytelen(msg->Data);
	msg->Header.ArgLen = bytelen(msg->Arg);
	return *msg;
}

Message Message::Deserialize(BYTE* data)
{
	Message *msg = new Message();
	memcpy(&(msg->Header),	 data, sizeof(MessageHeader));
	msg->Data = (BYTE*)(malloc(msg->Header.DataLen));
	memcpy(msg->Data, data + sizeof(MessageHeader), msg->Header.DataLen);
	if(msg->Header.ArgLen>0)
	{
		msg->Arg = (BYTE*)malloc(msg->Header.ArgLen);
		memcpy(msg->Arg, data + sizeof(MessageHeader) + msg->Header.DataLen, msg->Header.ArgLen);
	}
	else
		msg->Arg = 0;

	return *msg;
}

BYTE* Message::Serialize(Message message)
{
	BYTE* pBuf = (BYTE*) malloc (
		sizeof(MessageHeader) 		
		+ message.Header.DataLen 
		+ message.Header.ArgLen);
		
	memcpy(pBuf,&message.Header,sizeof(MessageHeader));	
	if(message.Data > 0)
		memcpy(pBuf + sizeof(MessageHeader), message.Data, message.Header.DataLen);	
	else 
		pBuf[sizeof(MessageHeader)] = 0;

	if(message.Arg > 0)
		memcpy(pBuf + sizeof(MessageHeader) + message.Header.DataLen, message.Arg, message.Header.ArgLen);
	else
		pBuf[sizeof(MessageHeader) + message.Header.DataLen] = 0;
	
	return pBuf;
}

Message::~Message()
{
	// �� ����� ���� ��������� � ����������
	//delete Data;
	//delete Arg;
}