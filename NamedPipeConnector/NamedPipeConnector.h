#include "stdafx.h"
#include "Connector.h"

#define PIPE_NUM 1
#define PIPE_MSG_BUFFER 1024
#define MAX_ADDRESS_SIZE 256
#define CONNECT_TIMEOUT 20000
#define WORKER_EVENT_OK_NAME L"hEvWorkerOk"
#define WORKER_EVENT_FAIL_NAME L"hEvWorkerFail"
#define WORKER_SRV_EVENT_OK_NAME L"hEvWorkerOkSRV"
#define WORKER_SRV_EVENT_FAIL_NAME L"hEvWorkerFailSRV"
#define WORKER_JOB_CONNECT 0
#define WORKER_JOB_SEND 1
#define WORKER_JOB_RECEIVE 2

struct WorkerData
{
	HANDLE* hPipe;
	HANDLE* hEvWorkerOk;
	HANDLE* hEvWorkerFail;

	BYTE* InBuffer; // Buffer for reading
	BYTE* OutBuffer; // Array of bytes for writing ro pipe
	DWORD nOutBufferBytes; // Number of bytes to write

	DWORD BytesWritten;
	DWORD BytesReaded;

	DWORD Job;
};

struct MemHeader
{	
	DWORD Size;
	char To[MAX_ADDRESS_SIZE];
	char From[MAX_ADDRESS_SIZE];
};

class NamedPipeConnector : Connector
{
public:
	NamedPipeConnector(void);
	virtual bool Connect(char address[], char peerName[]);
	virtual bool Send(char address[], Message message, DWORD timeout);
	virtual bool Receive(Message& message, DWORD timeout);
	virtual bool Close();	

private: 
	static DWORD WINAPI WorkerProc(LPVOID pData);

	bool isServer;
	char name[MAX_ADDRESS_SIZE];
	char pipeName[MAX_ADDRESS_SIZE];

	HANDLE hPipe;
	HANDLE hWorkerThreadHandle;	
	HANDLE* lpEvHandles;
	DWORD dwThreadId;
};

