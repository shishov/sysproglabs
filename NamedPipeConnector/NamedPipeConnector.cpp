#include "stdafx.h"
#include "NamedPipeConnector.h"

bool BytesToMessage(Message& message, BYTE* bytes)
{
	MemHeader* header = new MemHeader();	
	memcpy((void *)header,bytes,sizeof(MemHeader));
	if(header->Size)
	{
		BYTE* pMsgBuf = (BYTE*)malloc(header->Size);
		memcpy(pMsgBuf,bytes+sizeof(MemHeader),header->Size);
		message = Message::Deserialize(pMsgBuf);
		return true;
	}
	else		
		return false;
}

NamedPipeConnector::NamedPipeConnector()
{	
}

DWORD WINAPI NamedPipeConnector::WorkerProc(LPVOID pData)
{
	WorkerData* data = (WorkerData*)pData;
	switch (data->Job)
	{
	case WORKER_JOB_CONNECT:
		{
			if (ConnectNamedPipe(*(data->hPipe), NULL) == 0)					
				SetEvent(*(data->hEvWorkerFail));				
			else
				SetEvent(*(data->hEvWorkerOk));
			break;
		}
	case WORKER_JOB_SEND:
		{
			if(WriteFile(*(data->hPipe), data->OutBuffer, data->nOutBufferBytes, &(data->BytesWritten), NULL)==0)
				SetEvent(*(data->hEvWorkerFail));				
			else
				SetEvent(*(data->hEvWorkerOk));
			break;
		}
	case WORKER_JOB_RECEIVE:
		{
			while(ReadFile(*(data->hPipe), data->InBuffer, PIPE_MSG_BUFFER, &(data->BytesReaded),  NULL) > 0)
			{
				SetEvent(*(data->hEvWorkerOk));					
				return 0;
			}
			SetEvent(*(data->hEvWorkerFail));
			break;
		}
	default:
		break;
	}		
	return 0;
}

bool NamedPipeConnector::Connect(char address[], char peerName[])
{
	lpEvHandles = new HANDLE[2];
	lpEvHandles[0] = CreateEventW(0,0,0,WORKER_EVENT_OK_NAME);
	lpEvHandles[1] = CreateEventW(0,0,0,WORKER_EVENT_FAIL_NAME);

	sprintf_s(name,MAX_ADDRESS_SIZE,"%s",peerName);
	sprintf_s(pipeName,MAX_ADDRESS_SIZE,"%s",address);

	if ((hPipe = CreateNamedPipe(pipeName,
		PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE,
		PIPE_NUM, PIPE_MSG_BUFFER, PIPE_MSG_BUFFER, 1000, NULL)) == INVALID_HANDLE_VALUE)
	{
		// Connection as a server failed, trying to connect to existing pipe
		isServer = false;

		if (WaitNamedPipe(pipeName, NMPWAIT_WAIT_FOREVER) == 0)				
			return false;		

		if ((hPipe = CreateFile(pipeName,
			GENERIC_READ | GENERIC_WRITE, 0,
			(LPSECURITY_ATTRIBUTES) NULL, OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			(HANDLE) NULL)) == INVALID_HANDLE_VALUE)
			return false;	
		printf("Client Mode\n");
		return true;
	}
	else
	{
		// Connection as a server succeed, start waiting for clients
		isServer = true;		

		//UNIQUE eventNAMES for server
		lpEvHandles[0] = CreateEventW(0,0,0,WORKER_SRV_EVENT_OK_NAME);
		lpEvHandles[0] = CreateEventW(0,0,0,WORKER_SRV_EVENT_FAIL_NAME);

		WorkerData* pData = new WorkerData();
		pData->Job = WORKER_JOB_CONNECT;
		pData->hPipe = &hPipe;
		pData->hEvWorkerOk = &lpEvHandles[0];
		pData->hEvWorkerFail = &lpEvHandles[1];

		if ((hWorkerThreadHandle = CreateThread(NULL, 0, WorkerProc,
			pData, 0, &dwThreadId)) == NULL)			
			return false;		

		DWORD dwWaitResult = WaitForMultipleObjects(2,lpEvHandles,FALSE,CONNECT_TIMEOUT);
		if(dwWaitResult == WAIT_OBJECT_0 + 0)
		{
			printf("Server Mode\n");
			return true;
		}
		if(dwWaitResult == WAIT_OBJECT_0 + 1)
			return false;
		if(dwWaitResult == WAIT_TIMEOUT)
		{
			TerminateThread(hWorkerThreadHandle,0);
			CloseHandle(hWorkerThreadHandle);
			return false;
		}			
	}	
	return false;
}

bool NamedPipeConnector::Receive(Message& message, DWORD timeout)
{		
	BYTE* bytes = (BYTE*)malloc(PIPE_MSG_BUFFER);

	WorkerData* pData = new WorkerData();
	pData->Job = WORKER_JOB_RECEIVE;
	pData->hPipe = &hPipe;
	pData->hEvWorkerOk = &lpEvHandles[0];
	pData->hEvWorkerFail = &lpEvHandles[1];
	pData->InBuffer = &(*bytes);	

	if ((hWorkerThreadHandle = CreateThread(NULL, 0, WorkerProc,
		pData, 0, &dwThreadId)) == NULL)			
		return false;		

	DWORD dwWaitResult = WaitForMultipleObjects(2,lpEvHandles,FALSE,timeout);
	if(dwWaitResult == WAIT_OBJECT_0 + 0)
	{
		BytesToMessage(message,bytes);
		return true;
	}		
	if(dwWaitResult == WAIT_OBJECT_0 + 1)
		return false;
	if(dwWaitResult == WAIT_TIMEOUT)
	{
		TerminateThread(hWorkerThreadHandle,0);
		CloseHandle(hWorkerThreadHandle);
		return false;
	}
	return false;
}

bool NamedPipeConnector::Send(char address[], Message message, DWORD timeout)
{	
	BYTE* msgBytes = Message::Serialize(message);
	MemHeader* header = new MemHeader();
	header->Size = _msize(msgBytes);
	sprintf_s(header->From,MAX_ADDRESS_SIZE,"%s",name);
	BYTE* bytes = (BYTE*)malloc(sizeof(MemHeader) + header->Size);
	memcpy(bytes,(void*)header,sizeof(MemHeader));
	memcpy(bytes+sizeof(MemHeader),msgBytes,header->Size);

	WorkerData* pData = new WorkerData();
	pData->Job = WORKER_JOB_SEND;
	pData->hPipe = &hPipe;
	pData->hEvWorkerOk = &lpEvHandles[0];
	pData->hEvWorkerFail = &lpEvHandles[1];
	pData->OutBuffer = &(*bytes);
	pData->nOutBufferBytes = sizeof(MemHeader) + header->Size;		

	if ((hWorkerThreadHandle = CreateThread(NULL, 0, WorkerProc,
		pData, 0, &dwThreadId)) == NULL)			
		return false;		

	DWORD dwWaitResult = WaitForMultipleObjects(2,lpEvHandles,0,timeout);
	if(dwWaitResult == WAIT_OBJECT_0 + 0)
		return true;
	if(dwWaitResult == WAIT_OBJECT_0 + 1)
		return false;
	if(dwWaitResult == WAIT_TIMEOUT)
	{
		TerminateThread(hWorkerThreadHandle,0);
		CloseHandle(hWorkerThreadHandle);
		return false;
	}
	return false;
}

bool NamedPipeConnector::Close()
{	
	if(hPipe != 0) 
		CloseHandle(hPipe);
	if(hWorkerThreadHandle != 0) 
		CloseHandle(hWorkerThreadHandle);
	CloseHandle(lpEvHandles[0]);
	CloseHandle(lpEvHandles[1]);
	delete [] lpEvHandles;
	return true;
}

