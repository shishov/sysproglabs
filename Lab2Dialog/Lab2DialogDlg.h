
// Lab2DialogDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"
#include "Connector.h"


// ���������� ���� CLab2DialogDlg
class CLab2DialogDlg : public CDialogEx
{
// ��������
public:
	CLab2DialogDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_LAB2DIALOG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	void TryCreateThread();
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	int nStartThreads;
	CString messageText;
	afx_msg void OnBnClickedButton3();
	CListBox listBox;
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButton5();
	CString sAddress;
	CComboBox cConnectionType;
	BOOL controlClientLocally;
};
