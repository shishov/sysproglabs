
// Lab2DialogDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "Lab2Dialog.h"
#include "Lab2DialogDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAX_THREADS 200

// ���������� ���� CLab2DialogDlg
PROCESS_INFORMATION processInformation;
int threadCount = 0;
CListBox* pListBox;
Connector* connection;
bool localClient = false;

CLab2DialogDlg::CLab2DialogDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLab2DialogDlg::IDD, pParent)
	, nStartThreads(0)
	, messageText(_T(""))
	, sAddress(_T(""))
	, controlClientLocally(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLab2DialogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, nStartThreads);
	DDV_MinMaxInt(pDX, nStartThreads, 0, MAX_THREADS);
	DDX_Text(pDX, IDC_EDIT2, messageText);
	DDX_Control(pDX, IDC_LIST1, listBox);
	pListBox = &listBox;
	DDX_Text(pDX, IDC_EDIT3, sAddress);
	DDV_MaxChars(pDX, sAddress, 256);
	DDX_Control(pDX, IDC_COMBO3, cConnectionType);
	DDX_Check(pDX, IDC_CHECK1, controlClientLocally);
}

BEGIN_MESSAGE_MAP(CLab2DialogDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CLab2DialogDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CLab2DialogDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CLab2DialogDlg::OnBnClickedButton3)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON4, &CLab2DialogDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// ����������� ��������� CLab2DialogDlg

BOOL CLab2DialogDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������
		
	UpdateData();
	cConnectionType.InsertString(0,"Named Pipes");
	cConnectionType.InsertString(1,"Memory Mapped File");
	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CLab2DialogDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CLab2DialogDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

DWORD WaitForExit (LPVOID lpdwThreadParam ) 
{	
	DWORD dwEventWait = WaitForSingleObject(::processInformation.hProcess, INFINITE);	

	switch(dwEventWait)
		{
		case WAIT_OBJECT_0 + 0: 		
			{	
				while( pListBox->DeleteString(0) );
				CloseHandle( ::processInformation.hProcess );
				CloseHandle( ::processInformation.hThread );				
				break;
			}
		default:
			break;
		}

	return 0;
}

void CLab2DialogDlg::TryCreateThread()
{	
 	if(connection->Send("client", Message::Command(CommandType::CmdCreateThread), 1000))
	{
		Message reply;
		if(connection->Receive(reply,1000))
		{
			MessageBox((LPCSTR)reply.Data);
			UpdateData();
			CString t;
			t.Format(_T("Thread %d"), threadCount);
			pListBox->AddString(t);
			threadCount++;				
		}
		else
		{
			MessageBox("FAIL");
		}
	}
}

// OnPlusButon
void CLab2DialogDlg::OnBnClickedButton1()
{	
	DWORD dwExitCode = 0;
	if(localClient)
		GetExitCodeProcess(::processInformation.hProcess,&dwExitCode);

	if(dwExitCode == STILL_ACTIVE || !localClient)
	{	
		if(threadCount <= MAX_THREADS)
			TryCreateThread();
		else
			AfxMessageBox(_T("Maximum threads number reached"));
	} 
	else
	{
		STARTUPINFO startupInfo;
		memset(&::processInformation, 0, sizeof(::processInformation));
		memset(&startupInfo, 0, sizeof(startupInfo));
		startupInfo.cb = sizeof(startupInfo);

		TCHAR czCommandLine[] = _T("Lab2Console.exe");
		bool result = CreateProcess(0, czCommandLine, 0, 0, FALSE, 0, 0, 0,	
			&startupInfo, &::processInformation);
		if(!result)
		{
			AfxMessageBox(_T("Can't create process"));
			return;
		}

		threadCount = 1;
		
		UpdateData();
		pListBox->AddString(_T("All Threads"));
		pListBox->AddString(_T("Main Thread"));

		for (int i = 0; i < nStartThreads; i ++)
			TryCreateThread();

		DWORD dwThreadId;
		CreateThread(0,0,(LPTHREAD_START_ROUTINE)&WaitForExit,0,0,&dwThreadId);
	}
}

// OnStopButton
void CLab2DialogDlg::OnBnClickedButton2()
{	
	Message m = Message::Command(CommandType::CmdStopThread);
	if(threadCount > 0)
	{
		if (connection->Send("client",Message::Command(CommandType::CmdStopThread),1000)) 
		{	
			
			Message reply;
			if(connection->Receive(reply,1000))		
			{
				MessageBox((LPCSTR)reply.Data);
				threadCount--;
				pListBox->DeleteString(listBox.GetCount()-1);			
			} 
			else
				MessageBox("FAIL");				
		}
	} 
	else if(threadCount == 0)
	{
		if(connection->Send("client",Message::Command(CommandType::CmdStopProcess),1000))	
			while(pListBox->DeleteString(0) != LB_ERR);
	}
}

// OnSend
void CLab2DialogDlg::OnBnClickedButton3()
{		
	if(threadCount > 0)
	{
		UpdateData();
		int threadId = pListBox->GetCurSel()-2;
		char* arg = new char[sizeof(int)];
		sprintf_s( arg, sizeof(int), "%d", threadId );
		Message msg = Message::TextMessage((void*)((const char*)messageText),arg);
		if(connection->Send("client", msg, 1000))
		{
			Message reply;
			if(connection->Receive(reply,1000))		
				MessageBox((LPCSTR)reply.Data);
			else
				MessageBox("FAIL");
		}		
		else
			MessageBox("Failed to send");
	}
	else
		MessageBox("No threads");
}

void CLab2DialogDlg::OnClose()
{
	TerminateProcess(::processInformation.hProcess,0);
	CDialogEx::OnClose();
}

void CLab2DialogDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	TerminateProcess(::processInformation.hProcess,0);
}

// Connect Button
void CLab2DialogDlg::OnBnClickedButton5()
{
	/*
	Message m1 = Message::TextMessage("ShutDown","arg0");
	BYTE* bytes = Message::Serialize(m1);
	int a = _msize(bytes);	

	Message msg2 = Message::Deserialize(bytes);
	connection->Send("server", m1, 1000);		

	Message msg;
	connection->Receive(msg,1000);
	//MessageBox((LPCWSTR)msg.Data);

	BYTE* bytes2 = Message::Serialize(msg);
	a = _msize(bytes2);
	Message m2 = Message::Deserialize(bytes2);

	Message m1 = Message::TextMessage("Test text message","-1");
	connection->Send("client", m1, 1000);	
	*/

	UpdateData();
	HMODULE hLib;
	switch(cConnectionType.GetCurSel())
	{
	case 0:
		hLib = LoadLibrary(_T("NamedPipeConnector.dll"));
		break;
	case 1:
		hLib = LoadLibrary(_T("MemFileConnector.dll"));	
		break;
	default:
		{
			MessageBox("Select a connection type first!");
			return;
		}		
	}
		
	Connector* (*GetConnectorInstance)();
	(FARPROC &)GetConnectorInstance = GetProcAddress(hLib, "GetConnectorInstance");
	connection = GetConnectorInstance();	

	//connection->Connect("MMFFile","server");		
	//connection->Connect("\\\\.\\PIPE\\mypipe","server");	

	// Enable local process control or not
	localClient = controlClientLocally;

	if(connection->Connect(sAddress.GetBuffer(sAddress.GetLength()),"server"))
	{
		/*
		connection->Send(
			"client",
			Message::StatusMessage(
			MessageCode::Ok,
			"Ping"),
			1000);
		*/

		connection->Send(
			"client",
			Message::Command(CommandType::CmdAskThreads,0),
			1000);

		Message msg;
		if(connection->Receive(msg,1000))
		{
			if (!localClient)
			{
				while(pListBox->DeleteString(0) != LB_ERR);
				pListBox->AddString(_T("All Threads"));
				pListBox->AddString(_T("Main Thread"));

				int nThreads = atoi((const char*)msg.Arg);	
				for(int i = 0; i < nThreads; i++)
				{
					CString t;
					t.Format(_T("Thread %d"), threadCount);
					pListBox->AddString(t);
					threadCount++;			
				}
			}	
			MessageBox("Connected");
		}
		else
			MessageBox("Connection failed (no ping back)");
	}		
	else
		MessageBox("Connection failed");
}
