#include "stdafx.h"
#include "conio.h"
#include <iostream>
#include "windows.h"
#include <vector>
#include "Connector.h"

using namespace std;

class ThreadInfo
{	
public:
	ThreadInfo(int id) 
	{
		Id = id;
		char* buf = new char[100];
		sprintf_s(buf, 100, "CloseThreadEvent%d", id);
		hCloseThreadEvent = CreateEvent(0,0,0,buf);

		buf = new char[100];
		sprintf_s(buf, 100, "ReceiveEvent%d", id);
		hRecieveEvent = CreateEvent(0,0,0,buf);

		buf = new char[100];
		sprintf_s(buf, 100, "SuccessfullReceiveEvent%d", id);
		hSuccessfulRecieveEvent = CreateEvent(0,0,0,buf);		

		buf = new char[100];
		sprintf_s(buf, 100, "BufferMutex%d", id);
		hBufferMutex = CreateMutex(0,0,buf);
		delete buf;
	}
	~ThreadInfo()
	{		
		CloseHandle(hCloseThreadEvent);
		CloseHandle(hRecieveEvent);
		CloseHandle(hSuccessfulRecieveEvent);
	}

	bool StartThread(DWORD threadProcAddr, LPVOID param)
	{
		DWORD dwThreadId;
		if (CreateThread(0, 0,
			(LPTHREAD_START_ROUTINE) threadProcAddr, param, 0,
			&dwThreadId) == 0)
		{		
			return false;
		}
		else
		{
			return true;
		}		
	}
	bool StopThread()
	{
		SetEvent(hCloseThreadEvent);	
		return true;
	}
	bool Send(char* data)
	{
		buff = data;
		SetEvent(hRecieveEvent);
		DWORD dwEventWait = WaitForSingleObject(hSuccessfulRecieveEvent, INFINITE);
		return dwEventWait == WAIT_OBJECT_0;
	}
	char* GetBuffer()
	{
		DWORD dwMutexWait = WaitForSingleObject(hBufferMutex, INFINITE);
		if(dwMutexWait == WAIT_OBJECT_0)		
			return buff;

		return 0;
	}

	HANDLE hCloseThreadEvent;
	HANDLE hRecieveEvent;
	HANDLE hSuccessfulRecieveEvent;	
	int Id;

private:	
	HANDLE hBufferMutex;
	char* buff;
};

PROCESS_INFORMATION processInformation;
vector<ThreadInfo*> tiThreads;

void ProcessMessage(char* msg, int threadId)
{	
	FILE *file;
	char* path = new char[MAX_PATH];
	sprintf_s(path,MAX_PATH,"%d.log",threadId);
	errno_t err = fopen_s(&file,path,"w+");
	if (err == 0)
	{
		fprintf(file, "%s\n", msg);
		cout<<"Received message by thread ["<<threadId<<"]: "<<msg<<endl;
		fclose(file);
	}
	else
	{
		cout<<"Can't open file: "<<path<<endl;
	}
	delete path;
}

DWORD ThreadProc (LPVOID lpdwThreadParam ) 
{		
	ThreadInfo *tiThread = (ThreadInfo*)lpdwThreadParam;	
	bool running = true;

	HANDLE *lpHandles = new HANDLE[2];
	lpHandles[0] = tiThread->hCloseThreadEvent;
	lpHandles[1] = tiThread->hRecieveEvent;	
	
	cout<<"Initialized thread "<< tiThread->Id <<endl;

	while(running)
	{
		DWORD dwWaitResult = WaitForMultipleObjects(
			2,
			lpHandles,
			false,
			INFINITE
		);	

		switch(dwWaitResult)
		{	
		case WAIT_OBJECT_0: // Close Thread
			running = false;
			break;
		case WAIT_OBJECT_0 + 1: // Receive Thread
			{
				ProcessMessage(tiThread->GetBuffer(),tiThread->Id);									
				SetEvent(tiThread->hSuccessfulRecieveEvent);				
				break;
			}
		default:
			break;
		}
	}			
	
	cout<<"Exited thread"<< tiThread->Id <<endl;
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{	
	printf("Running...\n");
	printf("0 - Named pipes\n1 - Memory Mapped File\n");
	HMODULE hLib;
	int choice;
	cin>>choice;

	switch (choice)
	{
	case 0:
		hLib = LoadLibrary(_T("NamedPipeConnector.dll"));	
		break;
	case 1:
		hLib = LoadLibrary(_T("MemFileConnector.dll"));	
		break;
	default:
		{
			system("pause");
			return 0;
		}		
	}

	printf("Address [256]: ");
	char* address = new char[256];
	cin >> address;

	Connector* (*GetConnectorInstance)();
	(FARPROC &)GetConnectorInstance = GetProcAddress(hLib, "GetConnectorInstance");
	Connector* connection = GetConnectorInstance();		

	bool connectionResult = false;
	while(!connectionResult)
	{
		connectionResult = connection->Connect(address,"client");
		if(!connectionResult) 
		{
			printf("Failed to connect\n");		
			Sleep(100);
		}
	}

	bool isRun = true;
	
	Message msg;
	while(isRun)
	{		
		printf("Waiting for message...\n");
		if (connection->Receive(msg,INFINITE))
		{
			if(msg.Header.Type == MessageType::Command && atoi((const char*)msg.Data) == CommandType::CmdCreateThread)
			{				
				cout<<"Thread Created"<<endl;
				ThreadInfo* ti = new ThreadInfo(tiThreads.size()+1);
				
				if(ti->StartThread((DWORD)&ThreadProc,ti))
				{
					tiThreads.push_back(ti);		
					connection->Send(
						"server",
						Message::StatusMessage(
							MessageCode::Confirmation,
							"Thread created",
							&ti->Id),
						1000);
				}				
			}
			else if (msg.Header.Type == MessageType::Command && atoi((const char*)msg.Data) == CommandType::CmdStopThread)
			{
				if(tiThreads.back()->StopThread())
				{
					cout<<"Thread Stopped"<<endl;					
					connection->Send(
						"server",
						Message::StatusMessage(
							MessageCode::Confirmation,
							"Thread stopped"),
						1000);
					tiThreads.pop_back();
				}		
				else
				{
					cout<<"Failed to stop thread"<<endl;		
				}
			}
			else if (msg.Header.Type == MessageType::Command && atoi((const char*)msg.Data) == CommandType::CmdStopProcess)
			{
				isRun = false;				
			}	
			else if (msg.Header.Type == MessageType::StatusMessage) // ping
			{
				printf("Ping request\n");
				if(connection->Send(
					"server",
					Message::StatusMessage(
					MessageCode::Confirmation,
					"Ping back"),
					1000))
					printf("Pinged back\n");			
			}
			else if (msg.Header.Type == MessageType::TextMessage)
			{	
				bool success = false;
				int threadId = atoi((const char*)msg.Arg);												

				if(threadId == -2) 
				{
					for(int i = 0; i < tiThreads.size(); i++)
						tiThreads[i]->Send((char*)msg.Data);
					success = true;
				}

				if(threadId == -1) 
				{
					cout<<"Main thread message: "<<(char*)msg.Data<<endl;
					success = true;
				}
				
				if(threadId >= 0 && threadId < tiThreads.size())
				{
					success = tiThreads[threadId]->Send((char*)msg.Data);
				}

				if(success)
				{
					connection->Send(
						"server",
						Message::StatusMessage(
							MessageCode::Confirmation,
							"Message received"),
						1000);
				}
				else
				{
					cout<<"Failed to send message to thread "<<threadId<<endl;	
				}
			}
			else if(msg.Header.Type == MessageType::Command && atoi((const char*)msg.Data) == CommandType::CmdAskThreads)
			{
				printf("Threads count request\n");
				char* arg = new char[sizeof(int)];
				sprintf_s( arg, sizeof(int), "%d", tiThreads.size() );

				if(connection->Send(
					"server",
					Message::StatusMessage(MessageCode::Confirmation,"Sending threads count",arg),
					1000))
					printf("Sent threads count successfully\n");
			}
			else
			{
				cout<<"Don't understand the message"<<endl;
			}
		}
		else
		{			
			connection->Close();
			printf("Disconnected, waiting another connection...\n");		
			while(!connection->Connect(address,"client"));			
			printf("Client connected\n");		
		}
	}

	cout<<"Exiting"<<endl;
	connection->Send(
		"server",
		Message::StatusMessage(
			MessageCode::Confirmation,
			"Process exit"),
		1000);
	return 0;
}

