#include "stdafx.h"
#include "conio.h"
#include <iostream>
#include "windows.h"
#include <vector>

PROCESS_INFORMATION processInformation;
HANDLE hCreateThreadEvent = CreateEvent(NULL, FALSE, FALSE, _T("CreateThread"));
HANDLE hEventCreated = CreateEvent(NULL, FALSE, FALSE, _T("ThreadCreated"));

HANDLE hTerminateThreadEvent = CreateEvent(NULL, FALSE, FALSE, _T("TerminateThread"));
HANDLE hEventTerminated = CreateEvent(NULL, FALSE, FALSE, _T("ThreadTerminated"));

HANDLE hTerminateProcess = CreateEvent(NULL, FALSE, FALSE, _T("TerminateProcess"));
HANDLE hProcessTerminated = CreateEvent(NULL, FALSE, FALSE, _T("ProcessTerminated"));

std::vector<HANDLE> vHandles;

DWORD ThreadProc (LPVOID lpdwThreadParam ) 
{	
	HANDLE *hStop = (HANDLE*)lpdwThreadParam;	
	std::cout<<"Initialized thread"<<std::endl;
	DWORD dwEvent = WaitForSingleObject( *hStop, INFINITE );
	if(dwEvent ==  WAIT_OBJECT_0)
	{		
		std::cout<<"Exited thread"<<std::endl;
		SetEvent( ::hEventTerminated );
		CloseHandle(*hStop);
	}
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::cout<<"Running..."<<std::endl;

	
	DWORD dwThreadCount = 3;
	HANDLE *lpHandles = new HANDLE[dwThreadCount];
	lpHandles[0] = ::hCreateThreadEvent;
	lpHandles[1] = ::hTerminateThreadEvent;
	lpHandles[2] = ::hTerminateProcess;

	bool isRun = true;

	while(isRun)
	{
		DWORD dwEvent = WaitForMultipleObjects(
			dwThreadCount,
			lpHandles,
			false,
			INFINITE
		);	
		switch(dwEvent)
		{
		case WAIT_OBJECT_0 + 0: // CreateThread
			{
				DWORD dwThreadId;
				HANDLE hStopThread = CreateEvent(NULL, FALSE, FALSE, _T("StopThread"));
				if (CreateThread(0, 0,
					(LPTHREAD_START_ROUTINE) &ThreadProc,
					(LPVOID) &hStopThread,	0,
					&dwThreadId) == 0)
				{		
					std::cout<<"Can't start thread!"<<std::endl;
				}
				else
				{
					SetEvent(::hEventCreated);
					std::cout<<"Thread created"<<std::endl;
					::vHandles.push_back(hStopThread);
				}
				break;
			}
		case WAIT_OBJECT_0 + 1: // Terminate last thread
			{
				SetEvent(::vHandles.back());	
				::vHandles.pop_back();
				break;
			}
		case WAIT_OBJECT_0 + 2: // Terminate process
			{
				isRun = false;				
				break;
			}
		default:
			break;
		}
	}

	delete [] lpHandles;
	std::cout<<"Exiting"<<std::endl;		
	SetEvent(::hProcessTerminated);
	return 0;
}

