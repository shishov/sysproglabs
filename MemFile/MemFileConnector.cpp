#include "stdafx.h"
#include "MemFileConnector.h"
#include <time.h>

MemFileConnector::MemFileConnector()
{	
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	dwGranularity = si.dwAllocationGranularity;

	hRWMutex = CreateMutex(0,0,RWMutexName);
	hDataAvailableEvent = CreateEventW(0,true,0,(LPCWSTR)EventName);
}

bool MemFileConnector::Connect(char address[], char peerName[])
{
	sprintf_s(name,MAX_ADDRESS_SIZE,"%s",peerName);
	hFileMap = CreateFileMapping(
		INVALID_HANDLE_VALUE, 
		NULL, 
		PAGE_READWRITE, 
		0,
		dwGranularity,
		address);	
		
	return !(hFileMap == NULL || hFileMap == INVALID_HANDLE_VALUE);
}

bool MemFileConnector::Receive(Message& message, DWORD timeout)
{	
	bool success = false;	
	HANDLE *lpHandles = new HANDLE[2];	
	lpHandles[0] = hDataAvailableEvent;
	lpHandles[1] = hRWMutex;

	clock_t t = clock();
	while(!success && ((clock() - t) / CLOCKS_PER_SEC) < timeout)
	{	
		DWORD dwWaitResult = WaitForMultipleObjects(
				2,
				lpHandles,
				true,
				timeout
			);		
		if(dwWaitResult == WAIT_OBJECT_0)
		{	
			MemHeader* pHeader = (MemHeader*) MapViewOfFile(hFileMap, FILE_MAP_READ, 0, 0, HEADER_SIZE);			
			if(strcmp(pHeader->To,name) == 0)
			{
				ResetEvent(hDataAvailableEvent);
				BYTE* pBuf = (BYTE*) MapViewOfFile(hFileMap, FILE_MAP_READ, 0, 0, HEADER_SIZE + pHeader->Size );						
				BYTE* pMsgBuf = (BYTE*)malloc(pHeader->Size);
				memcpy(pMsgBuf, pBuf + HEADER_SIZE, pHeader->Size);		
				message = Message::Deserialize(pMsgBuf);				
				UnmapViewOfFile(pBuf);						
				delete pMsgBuf;
				success = true;
			}	
			else			
			{
				SetEvent(hDataAvailableEvent);				
			}
			
			UnmapViewOfFile(pHeader);				
		}

		ReleaseMutex(hRWMutex);

		if(dwWaitResult == WAIT_TIMEOUT)		
			break;		
	}

	delete lpHandles;
	return success;
}

bool MemFileConnector::Send(char address[], Message message, DWORD timeout)
{	
	MemHeader* pHeader = new MemHeader();
	BYTE* msgSer = Message::Serialize(message);
	pHeader->Size = _msize(msgSer);
	sprintf_s(pHeader->From,MAX_ADDRESS_SIZE,"%s",name);
	sprintf_s(pHeader->To,MAX_ADDRESS_SIZE,"%s",address);
	bool result = false;

	DWORD dwMutexWait = WaitForSingleObject(hRWMutex, timeout);
	if(dwMutexWait == WAIT_OBJECT_0)
	{
		BYTE* pBuf = (BYTE*) MapViewOfFile(hFileMap, FILE_MAP_WRITE, 0, 0, HEADER_SIZE + pHeader->Size);
		memcpy(pBuf,pHeader,HEADER_SIZE);
		memcpy(pBuf + HEADER_SIZE, msgSer, pHeader->Size);
		UnmapViewOfFile(pBuf);
		result = true;
	}
		
	delete pHeader;
	ReleaseMutex( hRWMutex );
	SetEvent( hDataAvailableEvent );		
	return result;
}

bool MemFileConnector::Close()
{		
	return CloseHandle(hFileMap);
}
