#include "stdafx.h"
#include "Connector.h"

#define MAX_ADDRESS_SIZE 16
#define HEADER_SIZE sizeof(MemHeader)

static const char RWMutexName[] = "MMFReadWrite";
static const char EventName[] = "MMFDataAvailable";

struct MemHeader
{	
	DWORD Size;
	char To[MAX_ADDRESS_SIZE];
	char From[MAX_ADDRESS_SIZE];
};

class MemFileConnector : Connector
{
public:
	MemFileConnector(void);
	virtual bool Connect(char address[], char peerName[]);
	virtual bool Send(char address[], Message message, DWORD timeout);
	virtual bool Receive(Message& message, DWORD timeout);
	virtual bool Close();	

private: 
	HANDLE hRWMutex;
	HANDLE hDataAvailableEvent;
	HANDLE hFileMap;
	DWORD dwGranularity;
	char name[MAX_ADDRESS_SIZE];
};

