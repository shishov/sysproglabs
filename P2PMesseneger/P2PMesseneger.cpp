// P2PMesseneger.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "Connector.h"
#include <iostream>
#include <string>
using namespace std;

HANDLE connectionMutex;
Connector* connection;

DWORD ThreadProc (LPVOID lpdwThreadParam ) 
{
	while (true)
	{
		Message msg;
		if(connection->Receive(msg,INFINITE))
		{
			cout<<msg.Data<<endl;
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	HMODULE hLib;
	hLib = LoadLibrary(_T("NamedPipeConnector.dll"));	
	Connector* (*GetConnectorInstance)();
	(FARPROC &)GetConnectorInstance = GetProcAddress(hLib, "GetConnectorInstance");
	connection = GetConnectorInstance();	
	connectionMutex = CreateMutex(0,0,_T("ConnectionMutex"));

	//cout<<"Login as: ";
	//char clientName[16];
	//cin>>clientName;
	/*
	if(connection->Connect("\\.\pipe\testpipe","client"))	
		cout<<"Successful login\n";
	else
	{
		cout<<"Err: Connection Problems\n";
		system("pause");
		return 0;
	}*/
	bool result = connection->Connect("\\\\.\\pipe\\testpipe","client");
	connection->Send("aasd",Message::TextMessage("test"),100);

	DWORD dwThreadId;
	if(!CreateThread(0, 0,
	(LPTHREAD_START_ROUTINE) &ThreadProc, 0, 0,
	&dwThreadId))
	{
		cout<<"Err: Can't Start listening Thread\n";
		system("pause");
		return 0;
	}

	while (true)
	{
		string address = "client";
		string msg = "test";

		cout<<"Send to: ";		
		getline(cin,address);

		cout<<"Message: ";
		getline(cin,msg);
		cout<<"Sending...\n";
		if(!connection->Send(
			(char*)address.c_str(),
			Message::TextMessage((char*)msg.c_str()),
			1000))
		{
			cout<<"Err: Can't Send the message\n";
			system("pause");
		}
	}

	return 0;
}

